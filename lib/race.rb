module RaceBet
  class Race

    class << self
      def score(guesses, winners)
        result = 0
        points = [15,10,5,3,1]
        
        guesses.each_with_index do |value,index|
          if value == winners[index]
            result += points[index]
          elsif winners[0..4].include? value
            result += 1
          end
        end
        result
      end
    end

  end
end
